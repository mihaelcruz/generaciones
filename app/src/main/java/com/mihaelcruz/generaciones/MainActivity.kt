package com.mihaelcruz.generaciones

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    btnVerificar.setOnClickListener{
        val edad= edtEdad.text.toString().toInt()

        val resultado = when(edad){
            in 1930..1948 -> {
                tvResultado.text = "Población: 6.300.000"
                imgTaxonomia.setImageResource(R.drawable.austeridad)
            }
            in 1949..1968 -> {
                tvResultado.text = "Población: 12.200.000"
                imgTaxonomia.setImageResource(R.drawable.ambicion)
            }
            in 1969..1980 -> {
                tvResultado.text = "Población: 9.300.000"
                imgTaxonomia.setImageResource(R.drawable.obsesion)
            }
            in 1981..1993 -> {
                tvResultado.text = "Población: 7.200.000"
                imgTaxonomia.setImageResource(R.drawable.frustracion)
            }
            in 1994..2010 -> {
                tvResultado.text = "Población: 7.800.000"
                imgTaxonomia.setImageResource(R.drawable.irreverencia)
            }
            else->{
                tvResultado.text = "No pertenece a ninguna generación"
                imgTaxonomia.setImageResource(R.drawable.ic_launcher_foreground)
            }
        }
    }

    }

}